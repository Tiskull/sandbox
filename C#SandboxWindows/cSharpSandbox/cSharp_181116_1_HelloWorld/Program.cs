﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharp_181116_1_HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words = new string[3] { "Hello", " ", "world !" };
            Console.WriteLine(new StringBuilder().Append(words[0]).Append(words[1]).Append(words[2]).ToString());
            Console.WriteLine("{0}{1}{2}", words[0], words[1], words[2]);
            Console.ReadKey();
        }
    }
}
