﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharp_181116_2_Scanner
{
    class Invoice
    {
        public string Name { get; set; }
        public double TVAPercent { get; set; }
        public int PreTaxesAmount { get; set; }
        private const double EXHANGE_RATE = 1.20; 

        public double CalculatePriceHTFR()
        {
            return PreTaxesAmount;
        }

        public double CalculatePriceHTUS()
        {
            return PreTaxesAmount * EXHANGE_RATE;
        }

        public double CalculatePriceTTCFR()
        {
            return PreTaxesAmount + PreTaxesAmount * (TVAPercent / 100);
        }

        public override string ToString()
        {
            return Name + ", " + TVAPercent + ", " + PreTaxesAmount;
        }
    }
}
