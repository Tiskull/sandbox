﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharp_181116_2_Scanner
{
    class Program
    {
        static void Main(string[] args)
        {
            Invoice invoice = new Invoice();
            Console.Write("Nom de la facture : ");
            invoice.Name = Console.ReadLine();
            Console.Write("Pourcentage TVA : ");
            invoice.TVAPercent = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            Console.Write("Montant hors-taxe de la facture : ");
            invoice.PreTaxesAmount = int.Parse(Console.ReadLine());
            Console.Write("HTEUR, HTUS, TTCEUR, ALL ? : ");
            string type;
            bool isTypeCorrect;
            do
            {
                type = Console.ReadLine();
                isTypeCorrect = true;
                switch (type)
                {
                    case "HTEUR": Console.WriteLine("HTEUR : " + invoice.CalculatePriceHTFR() + " EUR"); break;
                    case "HTUS": Console.WriteLine("HTUS : " + invoice.CalculatePriceHTUS() + " DOLLARS"); break;
                    case "TTCEUR": Console.WriteLine("TTCEUR : " + invoice.CalculatePriceTTCFR() + " EUR"); break;
                    case "ALL": Console.WriteLine("HTEUR : " + invoice.CalculatePriceHTFR() + " EUR"); Console.WriteLine("HTUS : " + invoice.CalculatePriceHTUS() + " DOLLARS"); Console.WriteLine("TTCEUR : " + invoice.CalculatePriceTTCFR() + " EUR"); break;
                    default: Console.WriteLine("Nous n'avons pas compris votre saisie, veuillez reessayer."); isTypeCorrect = false; break;
                }
            } while (!isTypeCorrect);
            Console.ReadKey();
        }
    }
}
