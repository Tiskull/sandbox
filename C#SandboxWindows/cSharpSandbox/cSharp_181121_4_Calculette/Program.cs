﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0004_Calculette
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isContinuying = true;
            int theOperator;
            int firstOperande;
            int secondOperande;
            while (isContinuying)
            {
                Console.Write("Modulo:1, SquareRoot:2, AbsoluteValue:3, Exit:0 ? ");
                int.TryParse(Console.ReadLine(), out theOperator);
                switch (theOperator)
                {
                    case 1:
                        Console.Write("Please provide the first operande : ");
                        int.TryParse(Console.ReadLine(), out firstOperande);
                        Console.Write("Please provide the second operande : ");
                        int.TryParse(Console.ReadLine(), out secondOperande);
                        Console.WriteLine(firstOperande+" % "+secondOperande+" = "+Calcul.CalculateModulo(firstOperande, secondOperande));
                        isContinuying = true;
                        break;
                    case 2:
                        Console.Write("Please provide the operande : ");
                        int.TryParse(Console.ReadLine(), out firstOperande);
                        Console.WriteLine("The square root of " + firstOperande + " is " + Calcul.CalculateSquareRoot(firstOperande));
                        isContinuying = true;
                        break;
                    case 3:
                        Console.Write("Please provide the operande : ");
                        int.TryParse(Console.ReadLine(), out firstOperande);
                        Console.WriteLine("The absolute value of " + firstOperande + " is " + Calcul.CalculateAbsoluteValue(firstOperande));
                        isContinuying = true;
                        break;
                    case 0: isContinuying = false; break;
                    default: Console.WriteLine("Please try again."); isContinuying = true; break;
                }
            }
        }
    }
}
