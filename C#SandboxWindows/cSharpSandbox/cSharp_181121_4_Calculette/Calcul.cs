﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0004_Calculette
{
    class Calcul
    {
        public static int CalculateAbsoluteValue(int firstOperande)
        {
            if (firstOperande<0)
            {
                Console.WriteLine("bou");
                return firstOperande*(-1);
            }
            return firstOperande;
        }

        public static int CalculateModulo(int firstOperande, int secondOperande)
        {
            int left = firstOperande;
            int compteurModulo = 1;
            while (left > secondOperande)
            {
                left = firstOperande - (secondOperande * compteurModulo);
                compteurModulo++;
            }
            return left;
        }

        public static float CalculateSquareRoot(int firstOperande)
        {
            int n=firstOperande;
            float tmp, sqt;
            sqt = n / 2;
            tmp = 0;
            while (sqt != tmp)
            {
                tmp = sqt;
                sqt = (n / tmp + tmp) / 2;
            }
            return sqt;
        }
    }
}
