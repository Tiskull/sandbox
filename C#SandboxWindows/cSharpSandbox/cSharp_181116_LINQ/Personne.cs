﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharp_181116_LINQ
{
    class Personne
    {
        public String Nom { get; set; }
        public String Prenom { get; set; }

        public Personne(string nom, string prenom)
        {
            Nom = nom;
            Prenom = prenom;
        }

        public override string ToString()
        {
            return Nom+", "+Prenom;
        }
    }
}
