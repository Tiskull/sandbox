﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharp_181116_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Personne> l = new List<Personne>();
            l.Add(new Personne("Guigon", "Julian"));
            l.Add(new Personne("Leuci", "Eva"));
            l.Add(new Personne("Guigon", "Margot"));
            l.Add(new Personne("Akcha", "Axel"));
            l.Add(new Personne("Urbaniak", "Loïs"));
            //LINQ
            IEnumerable<Personne> e = l.Where(p => p.Nom == "Guigon");
            foreach(Personne p in e)
            {
                Console.WriteLine(p);
            }
            Console.ReadKey();
        }
    }
}
