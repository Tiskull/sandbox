﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static _0009_SearchPersonne.DataAccess;

namespace _0009_SearchPersonne
{
    class Person
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public GenderEnum Gender { get; set; }
        public Adress Adress { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? DesactivatedAt { get; set; }

        public Person(int id, string uid, string lastName, string name, DateTime birthDate, GenderEnum gender, Adress adress, DateTime createdAt)
        {
            Id = id;
            Uid = uid;
            LastName = lastName;
            Name = name;
            BirthDate = birthDate;
            Gender = gender;
            Adress = adress;            
            CreatedAt = createdAt;
            DesactivatedAt = null;
        }

        /*
         * Calculate the age with BirthDate
         * */
        public int GetAge()
        {
            int d1 = DateTime.Now.Year;
            int d2 = BirthDate.Date.Year;
            return d1 - d2;
        }

        public override string ToString()
        {
            return new StringBuilder()
                .Append(Id).Append(", ")
                .Append(Uid).Append(", ")
                .Append(LastName).Append(", ")
                .Append(Name).Append(", ")
                .Append(BirthDate).Append(", ")
                .Append(Gender).Append(", ")
                .Append(Adress).Append(", ")
                .Append(CreatedAt.Date).ToString();
        }
    }
}
