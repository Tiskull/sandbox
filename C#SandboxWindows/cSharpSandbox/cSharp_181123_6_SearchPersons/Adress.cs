﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0009_SearchPersonne
{
    class Adress
    {
        public int StreetNumber { get; set; }
        public string Wording { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public Adress(int streetNumber, string wording, string postalCode, string city, string country)
        {
            StreetNumber = streetNumber;
            Wording = wording;
            PostalCode = postalCode;
            City = city;
            Country = country;
        }

        public override string ToString()
        {
            return new StringBuilder().Append(StreetNumber).Append(", ")
                .Append(Wording).Append(", ")
                .Append(PostalCode).Append(", ")
                .Append(City).Append(", ")
                .Append(Country).ToString();
        }
    }
}
