﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static _0009_SearchPersonne.DataAccess;

namespace _0009_SearchPersonne
{
    class Input
    {
        public static Adress InputAdress(string prompt)
        {
            Console.WriteLine(prompt);
            int streetNumber = InputInt("Street number : ");
            string wording = InputString("Wording : ");
            string postalCode = InputString("Postal code : ");
            string city = InputString("City : ");
            string country = InputString("Country : ");
            return new Adress(streetNumber, wording, postalCode, city, country);
        }

        public static int InputChoice(int options)
        {
            int choice = -1;
            bool isAGoodChoice = false;
            while (!isAGoodChoice)
            {
                isAGoodChoice = false;
                choice = InputInt("Your choice : ");
                if (choice >= 1 && choice <= options)
                {
                    isAGoodChoice = true;
                }
                else
                {
                    Console.WriteLine("Veuillez reessayer.");
                }
            }
            return choice;
        }

        public static DateTime InputDateTime(string prompt)
        {
            Console.WriteLine(prompt);
            int year = InputInt("Year : ");
            int month = InputInt("Month : ");
            int day = InputInt("Day : ");
            return new DateTime(year, month, day);
        }

        public static GenderEnum InputGender(string prompt)
        {
            Console.WriteLine(prompt);
            string input = InputString("(M : Man, W : Woman, O : Other) : ");
            while(input!="M" && input != "W" && input != "O")
            {
                Console.WriteLine("Veuillez reessayer.");
                input = InputString("(M : Man, W : Woman, O : Other) : ");
            }
            switch (input)
            {
                case "M": return GenderEnum.Man;
                case "W": return GenderEnum.Woman;
                case "O": return GenderEnum.Other;
                default: return GenderEnum.Other;
            }
        }

        //Bof Bof d'initialiser à -1 pour tester si nombre
        public static int InputInt(string message)
        {
            int input = -1;
            Console.Write(message);
            int.TryParse(Console.ReadLine(), out input);
            while (input == -1)
            {
                Console.WriteLine("Veuillez reessayer.");
                Console.Write(message);
                int.TryParse(Console.ReadLine(), out input);
            }
            return input;
        }

        public static Person InputPerson()
        {
            string lastName = InputString("Last name : ");
            string name = InputString("Name : ");
            DateTime birthDate = InputDateTime("--- Birth date ---");
            GenderEnum gender = InputGender("--- Gender ---");
            Adress adress = InputAdress("--- Adress ---");
            return new Person(-1, "", lastName, name, birthDate, gender, adress, DateTime.Now);
        }

        public static string InputString(string prompt)
        {
            Console.Write(prompt);
            string input = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("Veuillez reessayer.");
                Console.Write(prompt);
                input = Console.ReadLine();
            }
            return input;
        }
    }
}
