﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static _0009_SearchPersonne.Input;

namespace _0009_SearchPersonne
{
    class DataAccess
    {
        public enum GenderEnum { Man, Woman, Other };

        private int AutoIncrementalCounter;
        private IDictionary<int, List<Person>> Database { get; set; }

        public DataAccess()
        {
            AutoIncrementalCounter = 0;
            Database = new Dictionary<int, List<Person>>();

            //STUB
            this.AddPerson("GUIGON", "Julian", new DateTime(1997, 09, 04), GenderEnum.Man, new Adress(8, "chemin Louis chirpaz", "69130", "Ecully", "France"));
            this.AddPerson("GUIGON", "Julien", new DateTime(1997, 09, 04), GenderEnum.Man, new Adress(8, "chemin Louis chirpaz", "69130", "Ecully", "France"));
            this.AddPerson("AKCHU", "Axel", new DateTime(1997, 10, 30), GenderEnum.Man, new Adress(131, "cours Emile Zola", "69100", "Villeurbanne", "France"));
            this.AddPerson("GUIGON", "Margo", new DateTime(2001, 04, 21), GenderEnum.Woman, new Adress(225, "chemin de Gervais", "42110", "Civens", "France"));
            this.AddPerson("LEUCI", "Eva", new DateTime(1996, 10, 06), GenderEnum.Woman, null);
            //

            Work();
        }

        private bool ActivatePerson(int id, int age)
        {
            GetPerson(id, age).DesactivatedAt = null;
            return true;
        }

        private bool ActivatePerson(string lastName, string name, int age)
        {
            GetPerson(lastName, name, age).DesactivatedAt = null;
            return true;
        }

        /**
         * Add a Person to Database. 
         * If key do not exist, instanciate a new List<Person>.
         **/
        private bool AddPerson(string lastName, string name, DateTime birthDate, GenderEnum sex, Adress adress)
        {
            Person newPerson = CreatePerson(lastName, name, birthDate, sex, adress);
            List<Person> listPersons;
            int personAge = newPerson.GetAge();
            if (!Database.ContainsKey(personAge))
            {
                listPersons = new List<Person>();
                Database.Add(personAge, listPersons);
            }
            else
            {
                listPersons = Database[personAge];
            }
            listPersons.Add(newPerson);

            return true;
        }

        private Person CreatePerson(string lastName, string name, DateTime birthDate, GenderEnum sex, Adress adress)
        {
            DateTime createdAt = DateTime.Now;
            string Uid = GenerateUID(lastName, name, createdAt);
            Person newPerson = new Person(AutoIncrementalCounter, Uid, lastName, name, birthDate, sex, adress, createdAt);
            AutoIncrementalCounter++;
            return newPerson;
        }

        private bool DesactivatePerson(int id, int age)
        {
            GetPerson(id, age).DesactivatedAt = DateTime.Now;
            return true;
        }

        private bool DesactivatePerson(string lastName, string name, int age)
        {
            GetPerson(lastName, name, age).DesactivatedAt = DateTime.Now;
            return true;
        }

        private void DisplayDatabase()
        {
            int size = Database.Keys.Count;
            int i = 0;
            foreach (KeyValuePair<int, List<Person>> pair in Database)
            {
                Console.Write("{0}{1} : ", "{", pair.Key);
                DisplayList(pair.Value);
                Console.Write("}");
                if (i < size - 1)
                {
                    Console.Write(",");
                }
                i++;
            }
        }

        private void DisplayList(List<Person> listPersons)
        {
            int size = listPersons.Count;
            for (int i = 0; i < size; i++)
            {
                Person p = listPersons[i];
                if (p.DesactivatedAt == null)
                {
                    Console.Write("{");
                    Console.Write(p);
                    Console.Write("}");
                    if (i < size - 1)
                    {
                        Console.Write(",");
                    }
                }
            }
        }

        private void DisplayNominalScenario()
        {
            this.AddPerson("GUIGON", "Julian", new DateTime(1997, 09, 04), GenderEnum.Man, new Adress(8, "chemin Louis chirpaz", "69130", "Ecully", "France"));
            this.AddPerson("GUIGON", "Julien", new DateTime(1997, 09, 04), GenderEnum.Man, new Adress(8, "chemin Louis chirpaz", "69130", "Ecully", "France"));
            this.AddPerson("AKCHU", "Axel", new DateTime(1997, 10, 30), GenderEnum.Man, new Adress(131, "cours Emile Zola", "69100", "Villeurbanne", "France"));
            this.AddPerson("GUIGON", "Margo", new DateTime(2001, 04, 21), GenderEnum.Woman, new Adress(225, "chemin de Gervais", "42110", "Civens", "France"));
            this.AddPerson("LEUCI", "Eva", new DateTime(1996, 10, 06), GenderEnum.Woman, null);
            Console.WriteLine("Search person with id = 0 : ");
            Console.WriteLine(this.GetPerson(0, 21));
            Console.WriteLine();
            //Console.WriteLine(this.GetPerson("JUNET","Cédric", 21));
            //Console.WriteLine();
            Console.WriteLine("Search persons with I in their last name : ");
            Display.DisplayList<Person>(this.GetPersons("I"));
            Console.WriteLine("\n");
            Console.WriteLine("Updates LEUCI Eva, AKCHU Axel and GUIGON Margo");
            this.UpdatePersonBirthDate("LEUCI", "Eva", 22, new DateTime(1996, 10, 05));
            this.UpdatePersonLastName("AKCHU", "Axel", 21, "AKCHA");
            this.UpdatePersonName("GUIGON", "Margo", 17, "Margot");
            Console.WriteLine("Activate and Desactivate LEUCI Eva");
            this.DesactivatePerson("LEUCI", "Eva", 22);
            this.ActivatePerson("LEUCI", "Eva", 22);
            Console.WriteLine("Desactivate GUIGON Julien");
            this.DesactivatePerson("GUIGON", "Julien", 21);
            Console.WriteLine("Display all");
            this.DisplayDatabase();
            Console.WriteLine("\n");
        }

        private bool DisplayTerminal()
        {
            bool isQuitting = false;
            int options = 8, choice;
            Console.WriteLine("##################################################");
            Console.WriteLine("### 1 : Display database                       ###");
            Console.WriteLine("### 2 : Display one person                     ###");
            Console.WriteLine("### 3 : Display persons                        ###");
            Console.WriteLine("### 4 : Add a new person to the database       ###");
            Console.WriteLine("### 5 : Desactivate a person from the database ###");
            Console.WriteLine("### 6 : Reactivate a person from the database  ###");
            Console.WriteLine("### 7 : Update a person from the database      ###");
            Console.WriteLine("### 8 : Quit                                   ###");
            Console.WriteLine("##################################################");
            switch (InputChoice(options))
            {
                case 1: this.DisplayDatabase(); Console.WriteLine(); break;
                case 2 :
                    Console.WriteLine("--- Type of search ---");
                    Console.Write("(age+id : 1, age+lastName+name : 2) : ");
                    choice = InputChoice(2);
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine(GetPerson(InputInt("id : "), InputInt("age : ")));
                            break;
                        case 2:
                            Console.WriteLine(GetPerson(InputString("last name : "), InputString("name : "), InputInt("age : ")));
                            break;
                    }
                    break;
                case 3 :
                    DisplayList(GetPersons(InputString("chain : ")));
                    Console.WriteLine();
                    break;
                case 4 :
                    Person tmp = InputPerson();
                    AddPerson(tmp.LastName, tmp.Name, tmp.BirthDate, tmp.Gender, tmp.Adress);
                    break;
                case 5 :
                    Console.WriteLine("--- Type of desactivation ---");
                    Console.Write("(age+id : 1, age+lastName+name : 2) : ");
                    choice = InputChoice(2);
                    switch (choice)
                    {
                        case 1:
                            DesactivatePerson(InputInt("id : "), InputInt("age : "));
                            break;
                        case 2:
                            DesactivatePerson(InputString("last name : "), InputString("name : "), InputInt("age : "));
                            break;
                    }
                    break;
                case 6 :
                    Console.WriteLine("--- Type of activation ---");
                    Console.Write("(age+id : 1, age+lastName+name : 2) : ");
                    choice = InputChoice(2);
                    switch (choice)
                    {
                        case 1:
                            ActivatePerson(InputInt("id : "), InputInt("age : "));
                            break;
                        case 2:
                            ActivatePerson(InputString("last name : "), InputString("name : "), InputInt("age : "));
                            break;
                    }
                    break;
                case 7 :
                    Console.WriteLine("--- Type of update ---");
                    Console.WriteLine("########################################################");
                    Console.WriteLine("### 1 : Update last name by age and id               ###");
                    Console.WriteLine("### 2 : Update last name by age, last name and name  ###");
                    Console.WriteLine("### 3 : Update name by age and id                    ###");
                    Console.WriteLine("### 4 : Update name by age, last name and name       ###");
                    Console.WriteLine("### 5 : Update birth date by age and id              ###");
                    Console.WriteLine("### 6 : Update birth date by age, last name and name ###");
                    Console.WriteLine("### 7 : Quit                                         ###");
                    Console.WriteLine("########################################################");
                    choice = InputChoice(7);
                    switch (choice)
                    {
                        case 1: UpdatePersonLastName(InputInt("id : "), InputInt("age : "), InputString("new last name : ")); break;
                        case 2: UpdatePersonLastName(InputString("last name : "), InputString("name : "), InputInt("age : "), InputString("new last name : ")); break;
                        case 3: UpdatePersonName(InputInt("id : "), InputInt("age : "), InputString("new name : ")); break;
                        case 4: UpdatePersonName(InputString("last name : "), InputString("name : "), InputInt("age : "), InputString("new name : ")); break;
                        case 5: UpdatePersonBirthDate(InputInt("id : "), InputInt("age : "), InputDateTime("new birth date : ")); break;
                        case 6: UpdatePersonBirthDate(InputString("last name : "), InputString("name : "), InputInt("age : "), InputDateTime("new birth date : ")); break;
                        case 7: isQuitting = true; break;
                    }
                    break;
                case 8 :
                    isQuitting=true; break;
            }
            return isQuitting;
        }

        private string GenerateUID(string lastName, string name, DateTime createdAt)
        {
            return new StringBuilder().Append(lastName).Append("-").Append(name).Append("_").Append(createdAt.Day).Append("-").Append(createdAt.Month).Append("-").Append(createdAt.Year).Append("_").Append(createdAt.Hour).Append("h").Append(createdAt.Minute).Append("m").Append(createdAt.Second).Append("s").ToString();
        }

        //Recherche en O(Database[age].Count())
        private Person GetPerson(int id, int age)
        {
            return Database[age].Single(value => value.Id == id);
        }

        //Recherche en O(Database[age].Count())
        private Person GetPerson(string lastName, string name, int age)
        {
            return Database[age].Single(value => (value.LastName == lastName && value.Name==name));
        }

        //Recherche en O(N)
        private List<Person> GetPersons(string chain)
        {
            List<Person> listPersons = new List<Person>();
            foreach(int key in Database.Keys)
            {
                listPersons.AddRange(Database[key].Where(val => val.LastName.ToLower().Contains(chain.ToLower())));
            }
            return listPersons;
        }

        private void TerminalWork()
        {
            bool isQuitting = false;
            isQuitting = DisplayTerminal();
            while (!isQuitting)
            {
                isQuitting = DisplayTerminal();
            }
        }

        private bool UpdatePersonBirthDate(int id, int age, DateTime newBirthDate)
        {
            Person p = GetPerson(id, age);
            p.BirthDate = newBirthDate;
            p.Uid = GenerateUID(p.LastName, p.Name, p.CreatedAt);
            return true;
        }

        private bool UpdatePersonBirthDate(string lastName, string name, int age, DateTime newBirthDate)
        {
            Person p = GetPerson(lastName, name, age);
            p.BirthDate = newBirthDate;
            return true;
        }

        private bool UpdatePersonLastName(int id, int age, string newLastName)
        {
            Person p = GetPerson(id, age);
            p.LastName = newLastName;
            p.Uid = GenerateUID(p.LastName, p.Name, p.CreatedAt);
            return true;
        }

        private bool UpdatePersonLastName(string lastName, string name, int age, string newLastName)
        {
            Person p = GetPerson(lastName, name, age);
            p.LastName = newLastName;
            p.Uid = GenerateUID(p.LastName, p.Name, p.CreatedAt);
            return true;
        }

        private bool UpdatePersonName(int id, int age, string newName)
        {
            Person p = GetPerson(id, age);
            p.Name = newName;
            p.Uid = GenerateUID(p.LastName, p.Name, p.CreatedAt);
            return true;
        }

        private bool UpdatePersonName(string lastName, string name, int age, string newName)
        {
            Person p = GetPerson(lastName, name, age);
            p.Name = newName;
            p.Uid = GenerateUID(p.LastName, p.Name, p.CreatedAt);
            return true;
        }

        private void Work()
        {
            try
            {
                Console.Write("Display nominal scenario ? (y/n) : ");
                string answer = Console.ReadLine();
                while (answer != "y" && answer != "n")
                {
                    Console.Write("Please try again.");
                    Console.Write("Display nominal scenario ? (y/n) : ");
                    answer = Console.ReadLine();
                }
                switch (answer)
                {
                    case "y": DisplayNominalScenario(); break;
                    case "n": TerminalWork();break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                TerminalWork();
            }
        }
    }
}
