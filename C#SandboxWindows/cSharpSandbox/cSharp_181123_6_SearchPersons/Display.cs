﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0009_SearchPersonne
{
    class Display
    {
        public static void DisplayList<T>(IList<T> listObject)
        {
            int size = listObject.Count;
            for (int i = 0; i < size; i++)
            {
                Object o = listObject[i];
                Console.Write("{0}{1}{2}","{",o,"}");
                if (i < size - 1)
                {
                    Console.Write(",");
                }
            }
        }

        public static void DisplayDictionnary<T, U>(IDictionary<T, U> dictionnaryObject)
        {
            int size = dictionnaryObject.Keys.Count;
            int i = 0;
            foreach (KeyValuePair<T, U> pair in dictionnaryObject)
            {
                Console.Write("{0}{1} : {2}{3}", "{", pair.Key, pair.Value, "}");
                if (i < size - 1)
                {
                    Console.Write(",");
                }
                i++;
            }
        }

        public static void DisplayDictionnary<T,U,V>(IDictionary<T, U> dictionnaryObject) where U : List<V>
        {
            int size = dictionnaryObject.Keys.Count;
            int i = 0;
            foreach (KeyValuePair<T, U> pair in dictionnaryObject)
            {
                Console.Write("{0}{1} : ", "{", pair.Key);
                DisplayList<V>(pair.Value);
                Console.Write("}");
                if (i < size - 1)
                {
                    Console.Write(",");
                }
                i++;
            }
        }
    }
}
