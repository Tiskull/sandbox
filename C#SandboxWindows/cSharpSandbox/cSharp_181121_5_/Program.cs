﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0007_Palindromes
{
    class Program
    {
        static void Main(string[] args)
        {
            Work();
        }

        static void Work()
        {
            string entry=null;
            Palindrome palindrome = null;
            try
            {
                Console.Write("Please enter a string : ");
                entry = Console.ReadLine();
                ValidateFormat(entry);
                palindrome = new Palindrome(entry);
                Console.WriteLine(palindrome.ToString());
                Console.ReadKey();
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                Work();
            }
        }

        private static void ValidateFormat(string entry)
        {
            char[] specialsCharacters = new char[] { '+', '-', '*', '/', '#', '!', '?' };
            if (string.IsNullOrWhiteSpace(entry))
            {
                throw new ArgumentNullException("Error : the string is empty.");
            }
            if (entry.Length < 3)
            {
                throw new ArgumentException("Error : you must type a string which have 3 or more characters.");
            }
            if (entry.Any(char.IsDigit))
            {
                throw new ArgumentException("Error : the string contains numbers.");
            }
            if (specialsCharacters.Contains(entry.Last()))
            {
                throw new ArgumentException("Error : the last character is a special character. (+,-,*,/,#,!,?)");
            }
            char? anotherChar = entry.FirstOrDefault(e => (!e.Equals(entry[0]) && !e.Equals('\0')));
            if (anotherChar == '\0' || anotherChar == null)
            {
                throw new ArgumentException("Error : your string contains only one character repeated.");
            }
        }
    }
}
