﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0007_Palindromes
{
    class Palindrome
    {
        public string Element { get; set; }
        public char[] ElementFragmented { get; set; }
        public string ElementReversed { get; set; }
        public char[] ElementFragmentedReversed { get; set; }

        public Palindrome(string element)
        {
            Element = element;
            Fragment();
            Reverse();
            ElementReversed = Join(ElementFragmentedReversed);
        }

        public Palindrome(char[] element)
        {
            Element = Join(element);
            ElementFragmented = element;
            Reverse();
            Join(ElementFragmentedReversed);
        }

        private void Fragment()
        {
            ElementFragmented = new char[Element.Length];
            int counter = 0;
            foreach (char c in Element)
            {
                ElementFragmented[counter] = c;
                counter++;
            }
            ElementFragmented[0] = char.ToLower(ElementFragmented[0]);
        }

        private void Reverse()
        {
            ElementFragmentedReversed = new char[Element.Length];
            int size = ElementFragmented.Length;
            int counter = 0;
            while (size > 0)
            {
                ElementFragmentedReversed[counter] = ElementFragmented[size - 1];
                counter++;
                size--;
            }
            ElementFragmentedReversed[0] = char.ToUpper(ElementFragmentedReversed[0]);
        }

        private string Join(char[] element)
        {
            string elementReversed = "";
            foreach (char c in element)
            {
                elementReversed = elementReversed + c;
            }
            return elementReversed;
        }

        public bool IsPalindrome()
        {
            ElementFragmented[0] = char.ToLower(ElementFragmented[0]);
            ElementFragmentedReversed[0] = char.ToLower(ElementFragmentedReversed[0]);
            for(int i = 0; i< ElementFragmented.Length; i++)
            {
                if (ElementFragmented[i] != ElementFragmentedReversed[i])
                {
                    return false;
                }
            }
            return true;
        }

        public string NextAlphabeticalPalindrome()
        {
            int i = 0;
            char[] nextElementFragmented = new char[Element.Length];
            foreach(char c in ElementFragmented)
            {
                if (char.ToUpper(c) == 'Z')
                {
                    nextElementFragmented[i] = 'A';
                }
                else
                {
                    nextElementFragmented[i] = (char)(char.ToUpper(c)+1);
                }
                i++;
                
            }
            for(int j=1;j< nextElementFragmented.Length; j++)
            {
                nextElementFragmented[j] = char.ToLower(nextElementFragmented[j]);
            }
            return (Join(nextElementFragmented));
        }

        public override string ToString()
        {
            return Element + ", " + ElementReversed + " : " + IsPalindrome()+". Hash : "+GetHashCode()+", Next : " + NextAlphabeticalPalindrome();
        }

        public override int GetHashCode()
        {
            int hash=0;
            foreach(char c in ElementFragmented)
            {
                int index = char.ToUpper(c) - 64;
                hash = hash + index;
            }
            return hash;
        }
    }
}
