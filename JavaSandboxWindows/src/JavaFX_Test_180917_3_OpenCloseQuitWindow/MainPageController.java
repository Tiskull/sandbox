package JavaFX_Test_180917_3_OpenCloseQuitWindow;

import java.io.IOException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 *
 * @author julia
 */
public class MainPageController {
    @FXML
    public void comportementOuvrir(){
        try{
            Stage s = new Stage();
            FXMLLoader fl = new FXMLLoader(getClass().getResource("MainPage.fxml"));
            fl.setController(new MainPageController());
            Parent root = fl.load();
            Scene scene = new Scene(root);

            s.setTitle("JavaFX Tests Unitaires");
            s.setScene(scene);
            s.show();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    @FXML
    private Button fermer;
    @FXML
    public void comportementFermer(){
        Stage s =(Stage)fermer.getScene().getWindow();
        s.close();
    }

    @FXML
    public void comportementQuitter(){
        Platform.exit();
    }
}
