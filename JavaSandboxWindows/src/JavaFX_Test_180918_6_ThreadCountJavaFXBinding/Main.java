package JavaFX_Test_180918_6_ThreadCountJavaFXBinding;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Utilisation des threads dans une GUI avec un binding sur les labels
 * @author julia
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) {
        try{
            Count c1 = new Count(7,1000);
            Count c2 = new Count(12,500);
            Thread t1 = new Thread(c1);
            Thread t2 = new Thread(c2);
            t1.start();
            t2.start();
            
            FXMLLoader fl = new FXMLLoader(getClass().getResource("MainPage.fxml"));
            fl.setController(new MainPageController(c1,c2));
            Parent root = fl.load();
            Scene scene = new Scene(root);

            stage.setTitle("JavaFX Tests Unitaires");
            stage.setScene(scene);
            stage.show();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
