package JavaFX_Test_180918_6_ThreadCountJavaFXBinding;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * 
 * @author julia
 */
public class Count implements Runnable {
    private final Integer start;
    private final Integer ms;
    private Integer value;
    private final IntegerProperty integerProperty;
    
    public Count(Integer start, Integer ms){
        this.start=start;
        this.ms=ms;
        integerProperty = new SimpleIntegerProperty(start);
    }
    
    @Override
    public void run() {
        for(value=start; value>0; value--){
            try {
                Thread.sleep(ms);
            } catch (InterruptedException ex) {
                Logger.getLogger(Count.class.getName()).log(Level.SEVERE, null, ex);
            }
            Platform.runLater(() -> { //Pour changer la partie graphique dans le thread non graphique
                getIntegerProperty().setValue(value);
            });
            //getStringProperty().setValue(i.toString()); //génère une erreur 
        }
    }

    /**
     * @return the stringProperty
     */
    public IntegerProperty getIntegerProperty() {
        return integerProperty;
    }
}
