package JavaFX_Test_180918_6_ThreadCountJavaFXBinding;

import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.IntegerProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 *
 * @author julia
 */
public class MainPageController {
    @FXML
    private Label threadOneLabel;
    private final IntegerProperty threadOneIntegerProperty;
    @FXML
    private Label threadTwoLabel;
    private final IntegerProperty threadTwoIntegerProperty;
    @FXML
    private Label sumLabel;
    private IntegerBinding sumIntegerBinding;
    
    public MainPageController(Count threadOne, Count threadTwo){
        threadOneIntegerProperty=threadOne.getIntegerProperty();
        threadTwoIntegerProperty=threadTwo.getIntegerProperty();
    }
    
    @FXML
    private void initialize(){
        sumIntegerBinding = new IntegerBinding(){
            {
                bind(threadOneIntegerProperty);
                bind(threadTwoIntegerProperty);
            }
            
            @Override
            protected int computeValue(){
                return threadOneIntegerProperty.getValue()+threadTwoIntegerProperty.getValue();
            }
        };
        threadOneLabel.textProperty().bind(threadOneIntegerProperty.asString());
        threadTwoLabel.textProperty().bind(threadTwoIntegerProperty.asString());
        sumLabel.textProperty().bind(sumIntegerBinding.asString());
    }
}
