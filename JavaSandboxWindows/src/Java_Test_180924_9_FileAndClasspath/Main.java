/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java_Test_180924_9_FileAndClasspath;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tiskull
 */
public class Main {
    public static void main(String[] args) {
        
        String classPath = System.getProperty("java.class.path",".");
        System.out.println(classPath.replace(":", "\n"));
        
        try{
            Class.forName("com.sandbox.javasandbox.Java.Test_180924_9_FileAndClasspath.Main");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        File f1 = new File("src/main/java/com/sandbox/javasandbox/Java/Test_180920_6_ScannerConsoleEtSingleton/ScannerConsoleSingleton.java");
        System.out.println(f1.exists());
    }
}
