package Java_Test_180920_4_SwitchOnRandom;

import java.util.Random;

/**
 *
 * @author julia
 */
public class Main {
    /**
     * 
     * @param args 
     */
    public static void main(String[] args){
        for(int i =0; i<10; i++){
            switch(random1()) {
                case 0:
                    System.out.println("Lelouch");
                    break;
                case 1:
                    System.out.println("1up");
                    break;
                case 2:
                    System.out.println("Rem&Ram");
                    break;
                case 3 :
                    System.out.println("Santa");
                    break;
            }
        }
    }
    
    /**
     * Les nombres générés sont des nombres < 1 genre 0.95 ou 0.756
     * @return 
     */
    static int random1(){
        Double d = (Math.random()*100)%4;
        return d.intValue();
    }
    
    /**
     * 
     * @return 
     */
    static int random2(){
        Random r = new Random();
        return r.nextInt(3);
    }
}
