/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180924_2_BadFindObjects;

import Hibernate_Test_180924_1_PersistObjects.Client;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Ce code est mauvais car si la liste de clients commence 
 * par un id=2, il ne retourne rien.
 * @author tiskull
 */
public class Main {
    public static void main(String[] args) {
        List<Client> l = new LinkedList<>();
        
        File f = new File("src/main/resources/hibernate.cfg.xml");
        Configuration c = new Configuration().configure(f);
        c.addAnnotatedClass(Client.class);
        SessionFactory sf = null;
        Session s = null;
        Transaction tx = null;
        try{
            sf = c.buildSessionFactory();
            s = sf.openSession();
            tx = s.beginTransaction();
            int i = 1;
            Client cl=(Client)s.get(Client.class, i);
            while(cl!=null){
                l.add(cl);
                i++;
                cl=(Client)s.get(Client.class, i);
            }
            
            i = 1;
            cl = (Client)s.load(Client.class, i);
            while(cl!=null){
                try{
                    i++;
                    cl=(Client)s.load(Client.class, i);
                    // load n'execute la query que si on se sert de l'objet
                    // enleve la ligne si dessous et tu seras bloqué dans
                    // une boucle infinie
                    System.out.println(cl);
                }catch(ObjectNotFoundException e){
                    break;
                }
            }
            tx.commit();
        }catch(HibernateException e){
            if(tx!=null){
                tx.rollback();
            }
            e.printStackTrace();
        }finally{
            if(s!=null){
                s.close();
            }
            if(sf!=null){
                sf.close();
            }
        }
        System.out.println(l);
    }
}
