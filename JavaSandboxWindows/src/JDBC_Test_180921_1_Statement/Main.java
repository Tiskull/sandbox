package JDBC_Test_180921_1_Statement;

import java.sql.*;

/**
 *
 * @author julia
 */
public class Main {
    //private static final String JDBC = "jdbc:mysql://localhost:3306/";  
    static final String URL = "jdbc:postgresql://localhost:5432/sandbox";
    static final String USER = "lambda";
    static final String PASS = "lambda";
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            Class.forName("org.postgresql.Driver");
            
            conn = DriverManager.getConnection(URL,USER,PASS);
            stmt = conn.createStatement();
            String sql = "SELECT id, nom, prenom, createdat FROM personne";
            rs = stmt.executeQuery(sql);
            while(rs.next()){
                Integer id = rs.getInt("id");
                String nom = rs.getString("nom");
                String prenom = rs.getString("prenom");
                Timestamp date = rs.getTimestamp("createdat");
                System.out.print("Id: " + id);
                System.out.print(", Nom: " + nom);
                System.out.print(", Prenom: " + prenom);
                System.out.println(", Date: " + date);
             }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            try{
                if(rs!=null){
                    rs.close();
                }
                if(stmt!=null){
                    stmt.close();
                }
                if(conn!=null){
                   conn.close();
               }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
