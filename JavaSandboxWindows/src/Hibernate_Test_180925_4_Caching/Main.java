/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180925_4_Caching;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;

/**
 * Config : 
 * https://www.youtube.com/watch?v=TCHm1h7rBmo&t=321s
 * @author tiskull
 */
public class Main {
    public static void main(String[] args) {
        List<Animal> l = new LinkedList<>();
        
//        l.add(new Animal("Chien"));
//        l.add(new Animal("Chat"));
//        l.add(new Animal("Lapin"));
        
        File f = new File("src/main/resources/hibernate.cfg.xml");
        Configuration c = new Configuration().configure(f);
        c.addAnnotatedClass(Animal.class);
        ServiceRegistry sr = new StandardServiceRegistryBuilder().applySettings(c.getProperties()).build();
        SessionFactory sf = null;
        Session s1 = null;
        Session s2 = null;
        Session s3 = null;
        Session s4 = null;
        Transaction tx = null;
        try{
            sf = c.buildSessionFactory(sr);
            s1 = sf.openSession();
            tx = s1.beginTransaction();
            for (Animal an : l) {
                s1.save(an);
            }
            tx.commit();
            
            tx = s1.beginTransaction();
            System.out.println("1 : "+s1.get(Animal.class,4));
            System.out.println("1 : "+s1.get(Animal.class,4));
            tx.commit();
            
            s2 = sf.openSession();
            tx = s2.beginTransaction();
            //cette requete ne génère pas 2 appels serveurs grace au caching
            System.out.println("2 : "+s2.get(Animal.class,4));
            tx.commit();
            
            s3 = sf.openSession();
            tx = s3.beginTransaction();
            Query q1 = s3.createQuery("from Animal where id=4",Animal.class);
            q1.setCacheable(true);
            System.out.println("3 : "+q1.uniqueResult());
            System.out.println("3 : "+q1.uniqueResult());
            tx.commit();
            
            s4 = sf.openSession();
            tx = s4.beginTransaction();
            Query q2 = s4.createQuery("from Animal where id=4",Animal.class);
            q2.setCacheable(true);
            System.out.println("4 : "+q2.uniqueResult());
            tx.commit();
            
        }catch(Exception e){
            if(tx!=null){
                tx.rollback();
            }
            e.printStackTrace();
        }finally{
            if(s1!=null){
                s1.close();
            }
            if(s2!=null){
                s2.close();
            }
            if(s3!=null){
                s3.close();
            }
            if(s4!=null){
                s4.close();
            }
            if(sf!=null){
                sf.close();
            }
        }
    }
}
