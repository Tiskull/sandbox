package Java_Test_180920_5_RecherchesObjet;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author julia
 */
public class Main {
    /**
     * 
     * @param args 
     */
    public static void main(String[] args){
        List<String> l = new LinkedList<>();// OU List<String> l = new ArrayList<>();
        l.add("a");
        l.add("b");
        l.add("c");
        l.add("d");
        System.out.println(exists2(l, "a"));
        System.out.println(exists2(l, "e"));
    }
    
    /**
     * 
     * @param l
     * @param element
     * @return 
     */
    static boolean exists1(List<String> l, String element){
        return l.contains(element);
    }
    
    /**
     * Avec un Iterator, on peut modifier la collection en meme temps qu'on la parcourt
     * Cela déclenche une exception avec un foreach classique
     * Par contre il ne faut pas faire un for dans un for avec des iterator car ça marche po a cause du next
     * @param l
     * @param element
     * @return 
     */
    static boolean exists2(List<String> l, String element){
        for (Iterator<String> it = l.iterator(); it.hasNext();) {
            if(it.next().equals(element)){
                return true;
            }
        }
        return false;
    }
    
    /*
    ((SuperCapteur)myCaptorList.get(captorSelected)).getListOfSubCaptors().forEach((c) -> {
        listViewSuperCapteur.getItems().addAll(c.toString()+" : "+((CapteurPondere)c).getCoefficient().toString());
    });
    */

}
