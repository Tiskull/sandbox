package Java_Test_180918_3_RecherchesPrimitives;

import java.util.Arrays;

/**
 * @author julia
 */
public class Main {
    /**
     * 
     * @param args 
     */
    public static void main(String[] args){
        int[] ints = {-9,14,37,102};
        System.out.println(exists3(ints, 102));
        System.out.println(exists3(ints, 36));
    }
    
    /**
     * Solution 1 : Utiliser une boucle foreach
     * +La solution fonctionne avec un 'petit' tableau
     * +La solution fonctionne avec un tableau vide
     * -La solution utilise l'api J2SE pour effectuer la recherche dichotomique.
     * -La solution fonctionne en un temps raisonnable avec 1 million d'items
     * @param ints
     * @param k
     * @return 
     */
    static boolean exists1(int[] ints, int k){
        for(int i : ints){
            if(i==k){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Solution 2 : Utiliser la classe Arrays
     * +La solution fonctionne avec un 'petit' tableau
     * +La solution fonctionne avec un tableau vide
     * +La solution utilise l'api J2SE pour effectuer la recherche dichotomique.
     * -La solution fonctionne en un temps raisonnable avec 1 million d'items
     * @param ints
     * @param k
     * @return 
     */
    static boolean exists2(int[] ints, int k){
        Arrays.sort(ints);
        int result = Arrays.binarySearch(ints, k);
        return result>=0;
    }
    
    /**
     * Solution 3 : Recherche dichotomique
     * +La solution fonctionne avec un 'petit' tableau
     * +La solution fonctionne avec un tableau vide
     * -La solution utilise l'api J2SE pour effectuer la recherche dichotomique.
     * +La solution fonctionne en un temps raisonnable avec 1 million d'items
     * @param ints
     * @param k
     * @return 
     */
    static boolean exists3(int[] ints, int k){
       int milieu;
       int bas=0;
       int haut=ints.length-1;
       while(bas<=haut){
           milieu = (bas+haut)/2;
           System.out.println(ints[milieu]+" vs "+k);
           if(ints[milieu]<k){
               bas=milieu+1;
           }else if(ints[milieu]>k){
               haut=milieu-1;
           }else{
               return true;
           }
       }
       return false;
    }
}

