package JavaFX_Test_180917_4_ImageView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * 1- Attention à ne pas instancier les variables annotées de @FXML
 * 2- Le code ci-dessous ne fonctionne pas
 * public MainPageController(){imageView.setImage(CONSTANT_IMAGE_FIREFOX);}
 * Il est nécessaire d'utiliser la fonction initialize annotée pour modifier la vue juste après sa création
 * @author julia
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) {
        try{
            FXMLLoader fl = new FXMLLoader(getClass().getResource("MainPage.fxml"));
            fl.setController(new MainPageController());
            Parent root = fl.load();
            Scene scene = new Scene(root);

            stage.setTitle("JavaFX Tests Unitaires");
            stage.setScene(scene);
            stage.show();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
