package JavaFX_Test_180917_4_ImageView;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/** 
 * @author julia
 */
public class MainPageController {
    @FXML
    private ImageView imageView;
    private static final Image CONSTANT_IMAGE = new Image("http://placehold.it/400x200", 400, 200, false, false);
    
    @FXML
    private void initialize(){
        imageView.setImage(CONSTANT_IMAGE);
    }
}
