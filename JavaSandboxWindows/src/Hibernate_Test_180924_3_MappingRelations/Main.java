/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180924_3_MappingRelations;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author tiskull
 */
public class Main {
    public static void main(String[] args) {
        Eleve e1 = new Eleve("urbaniak", "lois");
        Laptop lap1 = new Laptop("macOS",e1);
        List<Laptop> l0 = new LinkedList<>();
        List<Eleve> l1 = new LinkedList<>();
//        l0.add(lap1);
//        e1.setLaptop(l0);
//        l1.add(e1);
        
        File f = new File("src/main/resources/hibernate.cfg.xml");
        Configuration c = new Configuration().configure(f);
        c.addAnnotatedClass(Eleve.class);
        c.addAnnotatedClass(Laptop.class);
        SessionFactory sf = null;
        Session s = null;
        Transaction tx = null;
        try{
            sf = c.buildSessionFactory();
            s = sf.openSession();
            tx = s.beginTransaction();
            for (Laptop lap : l0) {
                s.save(lap);
            }
            for (Eleve e : l1) {
                s.save(e);
            }
            tx.commit();
        }catch(HibernateException e){
            if(tx!=null){
                tx.rollback();
            }
            e.printStackTrace();
        }finally{
            if(s!=null){
                s.close();
            }
            if(sf!=null){
                sf.close();
            }
        }
    }
}
