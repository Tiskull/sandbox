/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180924_3_MappingRelations;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author tiskull
 */
@Entity
public class Laptop implements Serializable {
    private static final long serialVersionUID = 6247553747169995461L;
    @Id
    @GeneratedValue
    private int idLaptop;
    private String os;
    @ManyToOne
    private Eleve eleve;

    public Laptop() {
    }

    public Laptop(String os) {
        this.os = os;
    }

    public Laptop(String os, Eleve eleve) {
        this.os = os;
        this.eleve = eleve;
    }
    
    public int getIdLaptop() {
        return idLaptop;
    }

    public void setIdLaptop(int idLaptop) {
        this.idLaptop = idLaptop;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public Eleve getEleve() {
        return eleve;
    }

    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    @Override
    public String toString() {
        return "Laptop{" + "idLaptop=" + idLaptop + ", os=" + os + ", eleve=" + eleve + '}';
    }
    
    
}
