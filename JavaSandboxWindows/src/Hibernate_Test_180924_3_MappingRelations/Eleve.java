/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180924_3_MappingRelations;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author tiskull
 */
@Entity
public class Eleve implements Serializable {
    private static final long serialVersionUID = 3471571856313676940L;
    @Id
    @GeneratedValue
    private int idEleve;
    private String nom;
    private String prenom;
    @OneToMany(mappedBy="eleve",fetch=FetchType.EAGER)
    private List<Laptop> laptop;

    public Eleve() {
    }

    public Eleve(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public Eleve(String nom, String prenom, List<Laptop> laptop) {
        this.nom = nom;
        this.prenom = prenom;
        this.laptop = laptop;
    }

    public int getIdEleve() {
        return idEleve;
    }

    public void setIdEleve(int idEleve) {
        this.idEleve = idEleve;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public List<Laptop> getLaptop() {
        return laptop;
    }

    public void setLaptop(List<Laptop> laptop) {
        this.laptop = laptop;
    }

    @Override
    public String toString() {
        return "Eleve{" + "idEleve=" + idEleve + ", nom=" + nom + ", prenom=" + prenom + '}';
    }
}
