package Java_Test_180920_6_ScannerConsoleEtSingleton;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Il faut réduire au maximum le nombre de Scanner avec un singleton, 
 * sinon ça créer des problèmes avec System.in
 * 
 * Explication :
 * You have more than one Scanner that you close, which closes the underlying InputStream, therefore 
 * another Scanner can no longer read from the same InputStream and a NoSuchElementException results. 
 * For console apps, use a single Scanner to read from System.in.
 * @author julia
 */
public class Main {
    public static void main(String[] args){
        Scanner s = ScannerConsoleSingleton.getInstance().getScanner();
        System.out.println("Veuillez entrer deux entiers : ");
        int i1 = readNumber(s);
        int i2 = readNumber(s);
        System.out.println(i1+" + "+i2+" = "+(i1+i2));
        s.close(); //fermeture de l'InputStream
        try(Scanner s2 = new Scanner(System.in);){
            s2.nextInt();
        }catch(Exception e){
            System.out.println("\nErreur voulue : ");
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * @param s
     * @return 
     */
    static int readNumber(Scanner s){
        boolean isValid = false;
        int number=-1;
        while(!isValid){
            try{
                number = s.nextInt();
                isValid=true;
            } catch (InputMismatchException e) {
                System.out.println("Veuillez entrer un entier.");
                s.next();
            }   
        }
        return number;
    }
}
