package Java_Test_180920_6_ScannerConsoleEtSingleton;

import java.util.Scanner;

/**
 *
 * @author julia
 */
public class ScannerConsoleSingleton {
    private static ScannerConsoleSingleton singleton;
    private Scanner scanner;
    
    public static ScannerConsoleSingleton getInstance(){
        if(singleton==null){
            singleton = new ScannerConsoleSingleton();
        }
        return singleton;
    }
    
    private ScannerConsoleSingleton(){
        scanner = new Scanner(System.in);
    }
    
    public Scanner getScanner(){
        return scanner;
    }
    
    public void setScanner(Scanner scanner){
        this.scanner = scanner;
    }
}
