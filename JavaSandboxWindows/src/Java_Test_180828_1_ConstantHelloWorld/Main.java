package Java_Test_180828_1_ConstantHelloWorld;

import java.io.IOException;

/**
 *
 * @author julia
 */
public class Main {
    public static String CONSTANT_MESSAGE = "Hello world";
    public static void main(String[] args) throws IOException{
        System.out.println(CONSTANT_MESSAGE);
        System.in.read();
    }
    
}
