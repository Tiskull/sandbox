package JavaFX_Test_180920_8_ListView;

import java.util.LinkedList;
import java.util.List;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

/**
 *
 * @author julia
 */
public class MainPageController {
    public MainPageController(){
        l.add("a");
        l.add("b");
        l.add("c");
        l.add("d");
    }
    
    @FXML
    public void initialize(){
        updateListView();
    }
    
    @FXML
    private ListView listView;
    private final List<String> l = new LinkedList();
    @FXML
    public void comportementListView(){
        l.add(listView.getSelectionModel().getSelectedItems().get(0).toString()+" bis");
        updateListView();
    }
    private void updateListView(){
        listView.setItems(observableArrayList(l));
    }
}
