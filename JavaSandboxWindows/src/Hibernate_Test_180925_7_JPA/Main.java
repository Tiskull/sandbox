/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180925_7_JPA;

import Hibernate_Test_180924_1_PersistObjects.Client;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * JPA : Java Persistence API
 * @author tiskull
 */
public class Main {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
        EntityManager em=emf.createEntityManager();
        Client c = em.find(Client.class, 1);
        
    }
}
