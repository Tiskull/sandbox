/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180925_6_SQL;

import Hibernate_Test_180924_1_PersistObjects.Client;
import java.io.File;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author tiskull
 */
public class Main {
    public static void main(String[] args) {
        File f = new File("src/main/resources/hibernate.cfg.xml");
        Configuration c = new Configuration().configure(f);
        c.addAnnotatedClass(Client.class);
        ServiceRegistry sr = new StandardServiceRegistryBuilder().applySettings(c.getProperties()).build();
        SessionFactory sf = null;
        Session s = null;
        Transaction tx = null;
        try{
            sf = c.buildSessionFactory(sr);
            s = sf.openSession();
            tx = s.beginTransaction();
            SQLQuery sq = s.createSQLQuery("select * from Client");
            sq.addEntity(Client.class);
            System.out.println(sq.list());
            tx.commit();
        }catch(Exception e){
            if(tx!=null){
                tx.rollback();
            }
            e.printStackTrace();
        }finally{
            if(s!=null){
                s.close();
            }
            if(sf!=null){
                sf.close();
            }
        }
    }
}
