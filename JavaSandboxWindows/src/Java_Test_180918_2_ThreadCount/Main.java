package Java_Test_180918_2_ThreadCount;

/**
 *
 * @author julia
 */
public class Main {
    public static void main(String[] args){
        Thread t1 = new Thread(new Count(7,1000));
        Thread t2 = new Thread(new Count(12,500));
        t1.start();
        t2.start();
    }
}
