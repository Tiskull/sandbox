package Java_Test_180918_2_ThreadCount;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author julia
 */
public class Count implements Runnable {
    private final int start;
    private final int ms;
    
    public Count(int start, int ms){
        this.start=start;
        this.ms=ms;
    }
    
    @Override
    public void run() {
        for(int i=start; i>0; i--){
            System.out.println(i);
            try {
                Thread.sleep(ms);
            } catch (InterruptedException ex) {
                Logger.getLogger(Count.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
