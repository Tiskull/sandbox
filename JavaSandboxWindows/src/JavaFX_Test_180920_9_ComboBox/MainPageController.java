package JavaFX_Test_180920_9_ComboBox;

import java.util.LinkedList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;

/**
 *
 * @author julia
 */
public class MainPageController {
    @FXML
    private ComboBox comboBox;
    @FXML
    public void comportementComboBox(){
        System.out.println(comboBox.getSelectionModel().getSelectedIndex());
    }
    
    @FXML
    public void comportementButton1(){
        comboBox.getSelectionModel().clearSelection();
    }
    
    @FXML
    public void comportementButton2(){
        comboBox.getItems().removeAll(comboBox.getItems());
        List<String> l = new LinkedList<>();
        l.add("d");
        l.add("e");
        l.add("f");
        ObservableList e = FXCollections.observableList(l);
        comboBox.setItems(e);
    }
}
