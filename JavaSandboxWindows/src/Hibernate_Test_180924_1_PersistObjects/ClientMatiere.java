/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180924_1_PersistObjects;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author tiskull
 */
@Embeddable
public class ClientMatiere implements Serializable {
    private static final long serialVersionUID = 7790098927286504875L;
    private final String mat1;
    private final String mat2;
    
    public ClientMatiere() {
        mat1="rien";
        mat2="rien";
    }
    
    public ClientMatiere(String mat1){
        this.mat1=mat1;
        mat2="rien";
    } 
    
    public ClientMatiere(String mat1, String mat2){
        this.mat1=mat1;
        this.mat2=mat2;
    }

    public String getMat1() {
        return mat1;
    }

    public String getMat2() {
        return mat2;
    }
}
