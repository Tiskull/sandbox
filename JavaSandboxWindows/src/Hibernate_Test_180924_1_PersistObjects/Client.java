/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180924_1_PersistObjects;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author tiskull
 */
@Entity
@Table(name="client")
public class Client implements Serializable {
    private static final long serialVersionUID = -2217559417953453609L;
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;
    @Column(name="nom")
    private String nom;
    @Column(name="prenom")
    private String prenom;
    @Transient //pas pris en compte
    private int age ;
    private ClientMatiere matieres;

    public Client(){}
    
    public Client(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
        matieres=new ClientMatiere();
    }
    
    public Client(String nom, String prenom, int age) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        matieres=new ClientMatiere();
    }

    public Client(String nom, String prenom, ClientMatiere matieres) {
        this.nom = nom;
        this.prenom = prenom;
        this.matieres = matieres;
    }

    public Client(String nom, String prenom, int age, ClientMatiere matieres) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.matieres = matieres;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ClientMatiere getMatieres() {
        return matieres;
    }

    public void setMatieres(ClientMatiere matieres) {
        this.matieres = matieres;
    }
    
    @Override
    public String toString() {
        return "Client{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom +'}';
    }
    
}
