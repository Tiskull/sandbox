/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate_Test_180924_1_PersistObjects;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author tiskull
 */
public class Main {
    public static void main(String[] args) {
        List<Client> l = new LinkedList<>();
        
//        l.add(new Client("Riviere","Eloi",new ClientMatiere("informatique","bière")));
//        l.add(new Client("Molle","Richard",new ClientMatiere("informatique","steam")));
//        l.add(new Client("Le Maitre","Eric",new ClientMatiere("informatique","légumes")));
//        l.add(new Client("Guigon","Julian",new ClientMatiere("informatique","dessin")));
//        l.add(new Client("Chaussy","Gillian",new ClientMatiere("manutention","chats")));
//        l.add(new Client("Plastrier","Lahiah",new ClientMatiere("sociologie","jeux de combat")));
//        l.add(new Client("Leuci","Eva",new ClientMatiere("infirmière","sa potate")));
//        l.add(new Client("Vernay","Julien",new ClientMatiere("informatique","informatique")));
//        l.add(new Client("Akcha","Axel",new ClientMatiere("audioprotèsiste","chats")));
//        l.add(new Client("Servajean","Nathan",new ClientMatiere("bio","bière")));
        
        File f = new File("src/main/resources/hibernate.cfg.xml");
        Configuration c = new Configuration().configure(f);
        c.addAnnotatedClass(Client.class);
        SessionFactory sf = null;
        Session s = null;
        Transaction tx = null;
        try{
            sf = c.buildSessionFactory();
            s = sf.openSession();
            tx = s.beginTransaction();
            for (Client cl : l) {
                s.save(cl);
            }
            tx.commit();
        }
        catch(HibernateException e){
            if(tx!=null){
                tx.rollback();
            }
            e.printStackTrace();
        }
        finally{
            if(s!=null){
                s.close();
            }
            if(sf!=null){
                sf.close();
            }
        }
    }
}
