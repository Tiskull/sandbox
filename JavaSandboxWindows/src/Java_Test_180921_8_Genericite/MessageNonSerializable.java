package Java_Test_180921_8_Genericite;

/**
 *
 * @author julia
 */
public class MessageNonSerializable {
    private String message;

    public MessageNonSerializable(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageNonSerializable{" + "message=" + message + '}';
    }
    
}
