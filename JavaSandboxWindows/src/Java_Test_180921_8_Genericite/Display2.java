package Java_Test_180921_8_Genericite;

import java.io.Serializable;

/**
 * Classe générique avec type variable controllé
 * @author julia
 * @param <E>
 */
public class Display2<E extends Serializable>{
    public void display(E element){
        System.out.println(element.toString());
    }
}
