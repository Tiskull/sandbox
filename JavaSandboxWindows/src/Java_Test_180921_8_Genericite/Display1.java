package Java_Test_180921_8_Genericite;

/**
 * Classe générique
 * @author julia
 * @param <E>
 */
public class Display1<E>{
    public void display(E element){
        System.out.println(element.toString());
    }
}