package Java_Test_180921_8_Genericite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * http://www.informit.com/articles/article.aspx?p=170176
 * @author julia
 */
public class Main {
    public static void main(String[] args) {
        //La notation entres chevrons, c'est la généricité
        ArrayList<String> l1 = new ArrayList<>();
        LinkedList<String> l2 = new LinkedList<>();
        
        //Classes génériques
        Display2<String> d1 = new Display2();
        d1.display("ahah");
        Display2<Integer> d2 = new Display2();
        d2.display(4);
        //Display2<MessageNonSerializable> d3 = new Display2(); //erreur : non serializable
        Display2<MessageSerializable> d3 = new Display2<>();
        d3.display(new MessageSerializable("uiiiii"));
        
        //Méthode générique
        System.out.println("1 vs 2 : "+max(1,2));
    }
    
    /**
     * Méthode générique
     * @param <E>
     * @param e1
     * @param e2
     * @return 
     */
    static <E extends Comparable> E max(E e1, E e2){
        if(e1.compareTo(e2)>0){
            return e1;
        }else{
            return e2;
        }
    }
    
    //Wildcards
    /**
     * -When no specific knowledge of the value of a type parameter is needed, it can be replaced by a 
     * wildcard. The wildcard alone, represented by a question mark, takes the value of the default bound 
     * of the corresponding formal type parameter, which is Object in following example:
     * @param <T>
     * @param to
     * @param from 
     */
    static <T> void add1(List<T> to, List<T> from) {
        for (Iterator<T> i = from.iterator(); i.hasNext(); ){
            to.add(i.next());
        }
    }
    
    /**
     * -A wildcard might include an upper bound, which states that the value of the type parameter must be 
     * equal to, or a descendent of the bound, as in the following:
     * @param <T>
     * @param to
     * @param from 
     */
    static <T> void add2(List<T> to, List<? extends T> from) {
        for (Iterator<? extends T> i = from.iterator(); i.hasNext(); ){
            to.add(i.next());
        }
    }
    
    /**
     * -A wildcard may alternately specify a lower bound, as in the following:
     * @param <T>
     * @param to
     * @param from 
     */
    static <T> void add3(List<? super T> to, List<T> from) {
        for (Iterator<T> i = from.iterator(); i.hasNext(); ){
            to.add(i.next());
        }
    }
}




