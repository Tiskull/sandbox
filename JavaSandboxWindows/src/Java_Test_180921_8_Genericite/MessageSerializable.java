package Java_Test_180921_8_Genericite;

import java.io.Serializable;

/**
 *
 * @author julia
 */
public class MessageSerializable implements Serializable {
    private static final long serialVersionUID = 269377921947017052L;
    
    private String message;

    public MessageSerializable(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageSerializable{" + "message=" + message + '}';
    }
}
