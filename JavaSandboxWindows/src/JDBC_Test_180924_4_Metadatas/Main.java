package JDBC_Test_180924_4_Metadatas;

import java.sql.*;

/**
 *
 * @author julia
 */
public class Main {
    static final String URL = "jdbc:postgresql://localhost:5432/sandbox";
    static final String USER = "lambda";
    static final String PASS = "lambda";
    public static void main(String[] args) {
        Connection conn = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        try {
            Class.forName("org.postgresql.Driver");
            
            conn = DriverManager.getConnection(URL,USER,PASS);
            
            DatabaseMetaData databaseMetaData = conn.getMetaData();
            rs1 = databaseMetaData.getTables(null, null, null, new String[]{"TABLE"});
            
            while (rs1.next()) {
                String s = rs1.getString("TABLE_NAME");
                System.out.println("TABLE_NAME : "+s);
                rs2 = databaseMetaData.getColumns(null, null, s, null);
                while (rs2.next()){
                    System.out.print("COLUMN_NAME : "+rs2.getString("COLUMN_NAME"));
                    System.out.print(", DATA_TYPE : "+rs2.getString("DATA_TYPE"));
                    System.out.print(", COLUMN_SIZE : "+rs2.getString("COLUMN_SIZE"));
                    System.out.print(", DECIMAL_DIGITS : "+rs2.getString("DECIMAL_DIGITS"));
                    System.out.print(", IS_NULLABLE : "+rs2.getString("IS_NULLABLE"));
                    System.out.println(", IS_AUTOINCREMENT : "+rs2.getString("IS_AUTOINCREMENT"));
                }
            }            
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            try{
                if(rs1!=null){
                    rs1.close();
                }
                if(rs2!=null){
                    rs2.close();
                }
                if(conn!=null){
                   conn.close();
               }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
