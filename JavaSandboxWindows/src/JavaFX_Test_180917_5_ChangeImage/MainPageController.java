package JavaFX_Test_180917_5_ChangeImage;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/** 
 * @author julia
 */
public class MainPageController {
    @FXML
    private ImageView imageView;
    
    @FXML
    private void initialize(){
        imageView.setImage(generateImage());
    }
    
    private Image generateImage(){
        return new Image("http://placehold.it/"+x+"x"+y, x, y, false, false);
    }
    
    private int x=400;
    @FXML
    private TextField xTextField;
    
    @FXML
    public void comportementXTextField(){
        try{
            if(Integer.parseInt(xTextField.getText())>=0){
               x=Integer.parseInt(xTextField.getText());
            }
            imageView.setImage(generateImage());
        }catch(Exception e){}
    }
    
    private int y=200;
    @FXML
    private TextField yTextField;
    @FXML
    public void comportementYTextField(){
        try{
            if(Integer.parseInt(yTextField.getText())>=0){
                y=Integer.parseInt(yTextField.getText());
            }    
            imageView.setImage(generateImage());
        }catch(Exception e){}
    }
}
