package JavaFX_Test_180921_11_BidirectionnalBinding;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author julia
 */
public class Main {
    public static void main(String[] args) {
        SimpleIntegerProperty intProp = new SimpleIntegerProperty();
        SimpleStringProperty textProp = new SimpleStringProperty();
        StringConverter<? extends Number> converter =  new IntegerStringConverter();

        Bindings.bindBidirectional(textProp, intProp, (StringConverter<Number>)converter);

        intProp.set(2);
        System.out.println(textProp);

        textProp.set("8");
        System.out.println(intProp);
    }
}
