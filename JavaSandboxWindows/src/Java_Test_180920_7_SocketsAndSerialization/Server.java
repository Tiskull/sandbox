package Java_Test_180920_7_SocketsAndSerialization;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Attention : 
 * -Lancez le serveur puis le client
 * -Ne mélangez pas l'ordre des instanciation dans le try with resources
 * -N'utilisez que les méthodes readObject et writeObject, car les autres marchent bof
 * -Utilisez des objets Serializables !
 * @author julia
 */
public class Server {
    public static void main(String[] args) {
        int port=1234;
        try(
            ServerSocket serverSocket = new ServerSocket(port);
            Socket socket = serverSocket.accept();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
        ){
            Object objet = objectInputStream.readObject(); 
            if(objet instanceof Message){
                Message message = (Message)objet;
                System.out.println(message.getMessage());
            }
            objectOutputStream.writeObject(new Message("yop"));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
