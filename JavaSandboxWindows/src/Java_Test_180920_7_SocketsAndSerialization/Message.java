package Java_Test_180920_7_SocketsAndSerialization;

import java.io.Serializable;

/**
 *
 * @author julia
 */
public class Message implements Serializable {
    private static final long serialVersionUID = 6974253908637253969L;
    
    private final String message;
    
    public Message(String message){
        this.message = message;
    }
    
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
}
